﻿using Coscine.CodeGen.CodeGenerator;
using Coscine.Configuration;

namespace Coscine.CodeGen;

public class Program
{
    public static void Main(string[] args)
    {
        new CoscineCodeGenerator(new ConsulConfiguration()).GenerateCode().GetAwaiter().GetResult();
    }

}
