﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Coscine.CodeGen.CodeGenerator;

public abstract class CodeGenerator
{
    #region Functionality
    public async Task GenerateCode()
    {
        var jarFileName = await GetClientGenerator();

        var keys = await GetApiNames();

        var outputPath = await GetOutputPath();

        if (!Directory.Exists(outputPath))
        {
            Directory.CreateDirectory(outputPath);
        }

        await RetrieveDefinitions(outputPath, jarFileName, keys);

        await WriteDefinitions(outputPath);
    }

    private async Task<string> RetrieveDefinitions(string outputPath, string jarFileName, IEnumerable<string> keys)
    {
        var domainName = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName;
        var hostName = Dns.GetHostName();

        foreach (var key in keys)
        {
            Console.WriteLine(key);

            var swaggerUrl = await GetSwaggerUrl(domainName, hostName, key);
            var command = GetGenerationCommand(outputPath, jarFileName, key, swaggerUrl);

            await ExecuteCommand(command);
        }

        return outputPath;
    }

    private async Task WriteDefinitions(string outputPath)
    {
        var combinationFile = outputPath + "/apis.ts";
        var indexFile = outputPath + "/index.ts";

        var combinationFileImports = new List<string>();
        var combinationFileExports = new List<string>();

        var concreteApiNames = new List<string>();

        var first = true;

        var apiRegex = new Regex("(?<= )(.*?)(?= extends BaseAPI)");

        foreach (var directory in Directory.GetDirectories(outputPath))
        {
            var apiName = directory[(directory.LastIndexOf(".") + 1)..];
            var directoryName = directory[(directory.LastIndexOf("\\") + 1)..];

            apiName = apiName.Replace("Resources", "Resource");
            apiName = apiName.Replace("Notices", "Notice");

            var apiContent = File.ReadAllText($"./{outputPath}/{directoryName}/api.ts");

            foreach (var apiFile in Directory.GetFiles($"./{outputPath}/{directoryName}/@coscine/api/"))
            {
                apiContent += File.ReadAllText(apiFile);
            }

            var apiImplementations = apiRegex.Matches(apiContent);

            var customBasePath = await GetCustomBasePath(directoryName);

            foreach (var apiImplementation in apiImplementations)
            {
                var concreteApiName = apiImplementation.ToString().Replace("class ", "");
                concreteApiNames.Add(concreteApiName);
                combinationFileImports.Add($"import {{ {concreteApiName}Factory }} from './{directoryName}/api';");
                if (first)
                {
                    first = false;
                    combinationFileImports.Add($"import {{ Configuration }} from './{directoryName}/configuration';");
                }
                combinationFileExports.Add($"{concreteApiName}: {concreteApiName}Factory(new Configuration({{ 'accessToken': accessToken }}), '{customBasePath}', axios)");
            }
        }

        combinationFileExports.Sort();
        concreteApiNames.Sort();

        await WriteCombinationFile(combinationFile, combinationFileImports, combinationFileExports);
        await WriteIndexFile(indexFile, concreteApiNames);
    }

    private async Task WriteCombinationFile(string combinationFile, IEnumerable<string> combinationFileImports, IEnumerable<string> combinationFileExports)
    {
        var combinationFileText = "import { AxiosInstance } from 'axios';\n\n";
        combinationFileText += string.Join('\n', combinationFileImports);
        combinationFileText += "\n\n";

        combinationFileText = await GetCustomCodeForCombinationFile(combinationFileText);

        combinationFileText += @"function implementations(axios?: AxiosInstance) {
  return {
    ";
        combinationFileText += string.Join(",\n    ", combinationFileExports);
        combinationFileText += "\n  };\n};\n\nexport default implementations;\n";

        await File.WriteAllTextAsync(combinationFile, combinationFileText);
    }

    private async Task WriteIndexFile(string indexFile, IEnumerable<string> concreteApiNames)
    {
        var indexFileText = @"import implementations from './apis';
import instance from './axios-basic';

const apis = implementations(instance);

";
        foreach (var concreteApiName in concreteApiNames)
        {
            indexFileText += $"export const {concreteApiName} = apis.{concreteApiName};\n";
        }
        indexFileText += "\nexport default apis;\n";

        await File.WriteAllTextAsync(indexFile, indexFileText);
    }

    internal Task ExecuteCommand(string command)
    {
        var startInfo = new ProcessStartInfo
        {
            FileName = @"C:\Windows\System32\cmd.exe",
            Arguments = "/c " + command,
            RedirectStandardOutput = true,
            RedirectStandardError = true,
            UseShellExecute = false,
            CreateNoWindow = true,
        };
        using (var process = new Process
        {
            StartInfo = startInfo
        })
        {
            process.Start();

            var outputTask = process.StandardOutput.ReadToEndAsync();

            var errorTask = process.StandardError.ReadToEndAsync();

            Task.WaitAll(outputTask, errorTask);

            Console.WriteLine(outputTask.Result);
            Console.WriteLine(errorTask.Result);

            return process.WaitForExitAsync();
        }
    }
    #endregion

    #region Abstract Methods
    public abstract Task<string> GetClientGenerator();

    public abstract Task<IEnumerable<string>> GetApiNames();

    internal abstract Task<string> GetOutputPath();

    internal abstract Task<string> GetSwaggerUrl(string domainName, string hostName, string key);

    internal abstract string GetGenerationCommand(string outputPath, string jarFileName, string key, string swaggerUrl);

    internal abstract Task<string> GetCustomBasePath(string directoryName);

    internal abstract Task<string> GetCustomCodeForCombinationFile(string combinationFileText);
    #endregion
}
