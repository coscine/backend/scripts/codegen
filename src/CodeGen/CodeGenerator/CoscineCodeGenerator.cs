using Coscine.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Coscine.CodeGen.CodeGenerator;

public class CoscineCodeGenerator : CodeGenerator
{
    private readonly IConfiguration _configuration;

    public CoscineCodeGenerator(IConfiguration configuration)
    {
        this._configuration = configuration;
    }

    public async override Task<string> GetClientGenerator()
    {
        var jarDownloadLink = await _configuration.GetStringAsync("coscine/local/codegen/jarlink", "https://repo1.maven.org/maven2/org/openapitools/openapi-generator-cli/7.1.0/openapi-generator-cli-7.1.0.jar");

        using var httpClient = new HttpClient();
        var jarFileName = await _configuration.GetStringAsync("coscine/local/codegen/jarpath", "./codegen.jar");

        var response = await httpClient.GetAsync(new Uri(jarDownloadLink));

        using (var stream = await response.Content.ReadAsStreamAsync())
        using (var fileStream = new FileStream(jarFileName, FileMode.Create))
        {
            await stream.CopyToAsync(fileStream);
        }

        return jarFileName;
    }

    public async override Task<IEnumerable<string>> GetApiNames()
    {
        var apiPrefix = "coscine/apis/";

        var keys = await _configuration.KeysAsync(apiPrefix);
        return keys.Select((entry) => entry.Split('/')[2]).Distinct().Where(x =>
            x == "Coscine.Api"
            || x == "Coscine.Api.STS"
        );
    }

    internal async override Task<string> GetOutputPath()
    {
        return await _configuration.GetStringAsync("coscine/local/codegen/outputpath", "Output");
    }

    internal override Task<string> GetSwaggerUrl(string domainName, string hostName, string key)
    {
        if (key == "Coscine.Api")
        {
            return Task.FromResult($"https://{hostName}.{domainName}/coscine/api/swagger/v2/swagger.json");
        }
        return Task.FromResult($"https://{hostName}.{domainName}/coscine/api/{key}/swagger/v1/swagger.json");
    }

    internal override string GetGenerationCommand(string outputPath, string jarFileName, string key, string swaggerUrl)
    {
        return $"java \"-Dio.swagger.parser.util.RemoteUrl.trustAll=true\" \"-Dio.swagger.v3.parser.util.RemoteUrl.trustAll=true\" -jar \"{jarFileName}\" generate -i \"{swaggerUrl}\" -g typescript-axios -o \"{outputPath}/{key}\" --additional-properties=useSingleRequestParameter=true,apiPackage=@coscine/api,modelPackage=@coscine/model,withSeparateModelsAndApi=true --skip-validate-spec";
    }

    internal override Task<string> GetCustomBasePath(string directoryName)
    {
        var appendedPath = directoryName == "Coscine.Api.STS"
                ? $"/api/{directoryName}"
                : "";
        return Task.FromResult(
            $"https://' + getHostName() + '/coscine{appendedPath}"
        );
    }

    internal override Task<string> GetCustomCodeForCombinationFile(string combinationFileText)
    {
        combinationFileText += "let accessToken = '';";

        // Keep it like that for formatting
        combinationFileText += @"
if (typeof window !== 'undefined') {
  // LocalStorage > Global Variables
  const localStorageToken = localStorage.getItem('coscine.authorization.bearer');
  if (localStorageToken) {
    accessToken = 'Bearer ' + localStorageToken;
  }
}

const getHostName = () => {
  const localStorageHostName = typeof window !== 'undefined' ? localStorage.getItem('coscine.api.hostname') : null;
  if (localStorageHostName) {
    return localStorageHostName;
  }
  let hostName = typeof window !== 'undefined' ? window.location.hostname : 'coscine.rwth-aachen.de';
  if (hostName.indexOf(':') !== -1) {
    if (hostName.indexOf('https://') !== -1) {
      hostName = hostName.replace('https://', '');
    }
    hostName = hostName.substr(0, hostName.indexOf(':'));
  }
  return hostName;
};

";
        return Task.FromResult(combinationFileText);
    }
}
